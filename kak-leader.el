;;; kak-leader.el --- leader keys for kakoune emacs -*- lexical-binding: t; -*-

;;; Version: 1.0.0
;; Keywords: kakoune, ryo
;; Package-Requires: ((emacs "27.2"))

;;; This file is NOT part of GNU Emacs.

;;; Commentary:

;; Provides leader key support based on DOOM Emacs leader keys.
;; Intended to be used with kakoune.el and kak-colon.
;; Can also work standalone.

;;; Code:

(require 'ryo-modal)
;; Following deps can be removed if using different commands.
(require 'projectile)
(require 'consult)
(require 'which-key)

;;looks like second level key still displays +prefix. Bug report?
;;SPC leader keys, only implementing very basic keys for now.
(defun kak-leader-setup ()
  "Set up leader keybinds."
	"TODO: rename fns, add descriptions to shortcuts"
  (ryo-modal-keys
		("SPC" (("f" (("f" find-file :name ".emacs.d files")
                  ("r" consult-recent-file :name "recent files");also try counsel-recentf
                  ("p" dotd :name "emacs.d files") ;lambdas don't work
                  ("x" quit-window :name "quit-window")
                  ("R" rename-file :name "rename-file"))
              :name "file")
            ("F" (("p" dotd_full :name "all .emacs.d files"))
              :name "All Files")
            ("p" (("f" projectile-find-file :name "projectile-find-file")
                  ("a" projectile-add-known-project :name "add project")
                  ("p" projectile-switch-project :name "switch project")
                  ("d" projectile-remove-known-project :name "remove project")
                  ("F" projectile-find-file-in-known-projects :name "find file in project")
                  ("i" projectile-project-info :name "project info"))
              :name "project"))
		 :name "leader key")
	)
)

;;I can improve this by removing junk entries (sessionxxxx, etc.) and opening the minibuffer instead of a full one. Use projectile-find-file
(defun dotd ()
   "Show files in .emacs.d."
	(interactive)
	(ido-find-file-in-dir user-emacs-directory)
)

(defun dotd_full ()
   "Recursively show all files in .emacs.d."
	(interactive)
	(projectile-find-file-in-directory user-emacs-directory)
)

(provide 'kak-leader)
;;; kak-leader.el ends here
